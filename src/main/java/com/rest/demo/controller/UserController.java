package com.rest.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rest.demo.model.User;
import com.rest.demo.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
//	@RequestMapping(method = RequestMethod.GET, value="", produces=MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public List<User> getllUsers(){
		return userService.findAll();
	}
	
//	@RequestMapping(method = RequestMethod.GET, value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody User getUserById(@PathVariable("userId") long userId) {
		return userService.getUserById(userId);
	}
	
	@DeleteMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable("userId") long userId) {
		return userService.deleteUser(userId);
	}
	
	@PostMapping(value="", produces=MediaType.APPLICATION_JSON_VALUE)
	public User insertUser(@RequestBody User user) {
		return userService.inserUser(user);
	}
	
	@PutMapping(value="/{userId}",produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody User updateUser(@RequestBody User user,
										 @PathVariable("userId") long userId) {
		user.setUserId(userId);
		return userService.updateUser(user);
	}

}
