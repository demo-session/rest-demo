package com.rest.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rest.demo.model.User;
import com.rest.demo.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	public List<User> findAll(){
		return userRepo.findAll();
	}
	
	public User getUserById(long userId) {
		return userRepo.findById(userId).orElse(null);
	}
	
	public ResponseEntity<HttpStatus> deleteUser(long userId) {
		try {
			userRepo.deleteById(userId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	public User inserUser(User user) {
		return userRepo.save(user);
	}
	
	public User updateUser(User user) {
		User dbUser = getUserById(user.getUserId());
		if(user.getUserName() != null)
			dbUser.setUserName(user.getUserName());
		if(user.getEmail() != null)
			dbUser.setEmail(user.getEmail());
		if(user.getPassword() != null)
			dbUser.setPassword(user.getPassword());
		
		return userRepo.save(dbUser);
	}

}
